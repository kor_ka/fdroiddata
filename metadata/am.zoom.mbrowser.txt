Categories:Internet
License:MIT
Web Site:
Source Code:https://github.com/chelovek84/mBrowser
Issue Tracker:https://github.com/chelovek84/mBrowser/issues

Auto Name:mBrowser
Summary:Web browser
Description:
Simple webview based browser. Touch back or menu buttons for options.
.

Repo Type:git
Repo:https://github.com/chelovek84/mBrowser

Build:10.0,10
    commit=ff4ee8b9f1a059f1ac737f9b347e6be9bc6215e6
    subdir=mBrowser

Build:11.0,11
    commit=0edd757ea4e0ac0fc90da2f9ffe11bebe1d5e015
    subdir=mBrowser

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:11.0
Current Version Code:11

