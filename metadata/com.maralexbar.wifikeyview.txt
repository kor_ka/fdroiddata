Categories:Security
License:GPLv3
Web Site:
Source Code:https://github.com/Maralexbar/Wifi-Key-View
Issue Tracker:https://github.com/Maralexbar/Wifi-Key-View/issues

Auto Name:Wifi Key View
Summary:View saved WiFi passwords
Description:
View saved WiFi passwords. Requires busybox and root access.
.

Requires Root:yes

Repo Type:git
Repo:https://github.com/Maralexbar/Wifi-Key-View

Build:1.0,1
    commit=7a7e9709c78fdd8eba8a9a244c50f11342cc99b3
    prebuild=sed -i -e '/private String RunAsRoot/s/String cmd/String[] cmd/g' -e '/aa=RunAsRoot/d' -e '/String aa/aaa=RunAsRoot(new String[]{ "su", "-c", "cat /data/misc/wifi/wpa_supplicant.conf" });' src/com/maralexbar/wifikeyview/MainActivity.java

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.0
Current Version Code:1

