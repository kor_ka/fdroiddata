Categories:Navigation
License:GPLv3
Web Site:https://github.com/herverenault/Self-Hosted-GPS-Tracker/blob/HEAD/README.md
Source Code:https://github.com/herverenault/Self-Hosted-GPS-Tracker
Issue Tracker:https://github.com/herverenault/Self-Hosted-GPS-Tracker/issues

Auto Name:Self-Hosted GPS Tracker
Summary:Track your device
Description:
Sends your GPS coordinates to your server. It's your data, do what you want
with it. Server part is available in the source repository aswell.
.

Repo Type:git
Repo:https://github.com/herverenault/Self-Hosted-GPS-Tracker

Build:1.1.3,6
    commit=v1.1.3.1
    prebuild=mv java src
    target=android-17

Build:1.2,7
    commit=v1.2
    prebuild=mv java src
    target=android-17

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.2
Current Version Code:7

